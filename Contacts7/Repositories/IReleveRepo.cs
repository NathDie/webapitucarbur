﻿using Entities.DTO;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contrats.Repositories
{
    public interface IReleveRepo
    {
        public IEnumerable<Releve> GetReleves(bool tracked);

        public Releve GetReleve(Guid id, bool tracked);

        public void Creer(Releve releve);

        public void Supprimer(Releve releve);
    }
}
