﻿using Entities.DTO;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contrats.Repositories
{
    public interface IEnStockRepo
    {
        public IEnumerable<EnStock> GetEnStocks(bool tracked);

        public EnStock GetEnStock(Guid id, bool tracked);

        public void Creer(EnStock enStock);

        public void Supprimer(EnStock enStock);
    }
}
