﻿using Entities.DTO;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contrats.Repositories
{
    public interface IUtilisateurRepo
    {
        public IEnumerable<Utilisateur> GetUtilisateurs(bool tracked);

        public Utilisateur GetUtilisateur(Guid id, bool tracked);

        public Utilisateur GetUtilisateurByLoginAndPassword(String login, String mdp, bool tracked);

        public void Creer(Utilisateur utilisateur);

        public void Supprimer(Utilisateur utilisateur);
    }
}
