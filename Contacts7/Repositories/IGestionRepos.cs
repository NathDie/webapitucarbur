﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contrats.Repositories
{
	public interface IGestionRepos
	{
		ICarburantRepo Carburants { get; }
		IUtilisateurRepo Utilisateurs { get; }
        IEnStockRepo EnStocks { get; }
		IMarqueRepo Marques { get; }
		IStationRepo Stations { get; }
		IReleveRepo Releves { get; }
		void Save();
	}

}
