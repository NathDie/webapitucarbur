﻿using Entities.DTO;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contrats.Repositories
{
    public interface ICarburantRepo
    {
        public IEnumerable<Carburant> GetCarburants(bool tracked);

        public Carburant GetCarburant(Guid id, bool tracked);

        public void Creer(Carburant carburant);

        public void Supprimer(Carburant carburant);
    }
}
