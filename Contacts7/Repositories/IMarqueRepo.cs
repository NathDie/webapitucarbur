﻿using Entities.DTO;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contrats.Repositories
{
    public interface IMarqueRepo
    {
        public IEnumerable<Marque> GetMarques(bool tracked);

        public Marque GetMarque(Guid id, bool tracked);

        public void Creer(Marque marque);

        public void Supprimer(Marque marque);

        public string GetLabelMarque(Guid id, bool tracked);
    }
}
