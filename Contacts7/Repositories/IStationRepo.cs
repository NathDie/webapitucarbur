﻿using Entities.DTO;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contrats.Repositories
{
    public interface IStationRepo
    {
        public IEnumerable<Station> GetStations(bool tracked);

        public Station GetStation(Guid? id, bool tracked);

        public void Creer(Station station);

        public void Supprimer(Station station);
        
    }
}
