﻿using Microsoft.EntityFrameworkCore;
using Entities.Models;
using Entities.Configs;

namespace Entities
{
	public class RepoContext : DbContext
	{

		public RepoContext(DbContextOptions options) : base(options)
		{
		}

		public DbSet<Marque> Marques { get; set; }
		public DbSet<Carburant> Carburants { get; set; }
		public DbSet<EnStock> EnStocks { get; set; }
		public DbSet<Utilisateur> Utilisateurs { get; set; }
		public DbSet<Station> Stations { get; set; }
		public DbSet<Releve> Releve { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			//builder.ApplyConfiguration(new ConfigMarque());
			//builder.ApplyConfiguration(new ConfigStation());
			//builder.ApplyConfiguration(new ConfigUtilisateur());
			//builder.ApplyConfiguration(new ConfigCarburant());
			//builder.ApplyConfiguration(new ConfigEnStock());
			builder.ApplyConfiguration(new ConfigReleve());
			//builder.ApplyConfiguration(new ConfigEmploye());
		}

	}
}
