﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
    public class StationDTO
    {
        public StationDTO(Guid id, string adresse_postale, string coordonees, Guid marqueId, string marque)
        {
            Id = id;
            Adresse_postale = adresse_postale;
            Coordonees = coordonees;
            MarqueId = marqueId;
            Marque = marque;
        }

        public Guid Id { get; set; }

        public string Adresse_postale { get; set; }
        public string Coordonees { get; set; }

        public Guid MarqueId { get; set; }

        public string Marque { get; set; } 
    }
}
