﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
    public class StationFiltreDTO
    {
        private string coordonnees;
        private string libelle;

        public StationFiltreDTO(Guid? id, string adresse_postale, string coordonnees, Guid? marqueId, string libelle)
        {
            Id = id;
            Adresse_postale = adresse_postale;
            this.coordonnees = coordonnees;
            MarqueId = marqueId;
            this.libelle = libelle;
        }

        public StationFiltreDTO(Guid? id, string adresse_postale, string coordonees, Guid? marqueId, double? distance, string marque)
        {
            Id = id;
            Adresse_postale = adresse_postale;
            Coordonees = coordonees;
            MarqueId = marqueId;
            Marque = marque ; 
            Distance = distance;
        }

        public Nullable<Guid> Id { get; set; }
        public string Adresse_postale { get; set; }

        public string Coordonees { get; set; }

        public Nullable<Guid>  MarqueId { get; set; }

        public string Marque { get; set; }
        public Nullable<double> Distance { get; set; }

        public Nullable<int> PrixParLitre { get; set; }

        public Nullable<DateTime> DateReleve { get; set; }



    }
}