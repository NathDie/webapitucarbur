﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
    public class StationDetailDTO
    {
        public StationDetailDTO(Guid id, string adresse_postale, string coordonees, Guid marqueId, string marque, List<CarburantDTO> stationCarburants)
        {
            Id = id;
            Adresse_postale = adresse_postale;
            Coordonees = coordonees;
            MarqueId = marqueId;
            Marque = marque;
            StationCarburants = stationCarburants;
        }

        public Guid Id { get; set; }

        public string Adresse_postale { get; set; }
        public string Coordonees { get; set; }

        public Guid MarqueId { get; set; }

        public string Marque { get; set; }

        public List<CarburantDTO>  StationCarburants { get; set; } 

    }
}
