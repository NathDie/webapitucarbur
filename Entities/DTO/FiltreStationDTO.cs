﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
   public class FiltreStationDTO
    {
        public string Coord { get; set; }
        public  Nullable<double>  DistMax { get; set; }

        public Nullable<Guid> CarburantId{ get; set; }
        public  Nullable<Guid> UtilisateurId { get; set; }

    }
}
