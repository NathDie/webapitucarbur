﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
    public class UtilisateurCreationDTO
    {
        public string Login { get; set; }
        public string Mdp { get; set; }
    }
}
