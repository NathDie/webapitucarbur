﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
    public class MarqueDTO
    {
        public Guid Id { get; set; }
        public string Nom { get; set; }
    }
}
