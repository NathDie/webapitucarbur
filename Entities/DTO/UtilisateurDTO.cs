﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
    public class UtilisateurDTO
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Mdp { get; set; }
        public Guid StationId { get; set; }

    }
}
