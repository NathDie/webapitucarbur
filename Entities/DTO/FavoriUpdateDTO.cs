﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
    public class FavoriUpdateDTO
    {
        public Guid fUtilisateurId { get; set; }
        public Guid StationId { get; set; }
    }
}
