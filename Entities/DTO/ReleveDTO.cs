﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
	public class ReleveDTO
	{
		public Guid Id { get; set; }
		public Guid UtilisateurId { get; set; }
		public Guid CarburantId { get; set; }
		public Guid StationId { get; set; }
		public string PrixParLitre { get; set; }
		public DateTime DateReleve { get; set; }
		public double PrixConstate { get; set; }
	}
}
