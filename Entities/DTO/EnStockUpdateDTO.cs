﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
	public class EnStockUpdateDTO
	{
		
		public Guid StationId { get; set; }

		public Guid CarburantId { get; set; }

		public int PrixParLitre { get; set; }
	}
}
