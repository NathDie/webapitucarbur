﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DTO
{
	 public class CarburantDTO
	{
        private Guid guid;

        public CarburantDTO(Guid guid)
        {
            this.guid = guid;
        }

        public CarburantDTO(Guid id, string nom, string code_europeen, int price)
        {
            Id = id;
            Nom = nom;
            Code_europeen = code_europeen;
            this.price = price;
        }

        public Guid Id { get; set; }
		public string Nom { get; set; }
		public string Code_europeen { get; set; }

		public int price { get; set; }
	}
}
