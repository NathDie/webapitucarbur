﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configs
{
	public class ConfigEnStock : IEntityTypeConfiguration<EnStock>
	{
		public void Configure(EntityTypeBuilder<EnStock> builder)
		{
			builder.HasData
			(
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 30, DateMiseAJour = new DateTime(2021, 7 ,10 ,7 ,10, 24) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 2, DateMiseAJour = new DateTime(2017, 4, 14, 7, 8, 25) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 5, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 1, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },

				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), StationId = new Guid("44440460-5a3c-4265-99ac-5f804a40a278"), PrixParLitre = 11, DateMiseAJour = new DateTime(2021, 7, 10, 7, 10, 24) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), StationId = new Guid("44440460-5a3c-4265-99ac-5f804a40a278"), PrixParLitre = 2, DateMiseAJour = new DateTime(2017, 4, 14, 7, 8, 25) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), StationId = new Guid("44440460-5a3c-4265-99ac-5f804a40a278"), PrixParLitre = 25, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), StationId = new Guid("44440460-5a3c-4265-99ac-5f804a40a278"), PrixParLitre = 10, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },

				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), StationId = new Guid("03759557-1e0b-40b5-931e-e49e695ce698"), PrixParLitre = 3, DateMiseAJour = new DateTime(2021, 7, 10, 7, 10, 24) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), StationId = new Guid("03759557-1e0b-40b5-931e-e49e695ce698"), PrixParLitre = 02, DateMiseAJour = new DateTime(2017, 4, 14, 7, 8, 25) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), StationId = new Guid("03759557-1e0b-40b5-931e-e49e695ce698"), PrixParLitre = 13, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), StationId = new Guid("03759557-1e0b-40b5-931e-e49e695ce698"), PrixParLitre = 10, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },

				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), StationId = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"), PrixParLitre = 65, DateMiseAJour = new DateTime(2021, 7, 10, 7, 10, 24) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), StationId = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"), PrixParLitre = 2, DateMiseAJour = new DateTime(2017, 4, 14, 7, 8, 25) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), StationId = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"), PrixParLitre = 25, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), StationId = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"), PrixParLitre = 10, DateMiseAJour = new DateTime(2020, 8, 10, 5, 10, 30) },



				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 2, DateMiseAJour = new DateTime(2019, 5, 13, 6, 9, 19) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 2, DateMiseAJour = new DateTime(2017, 4, 14, 7, 8, 25) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 2, DateMiseAJour = new DateTime(2019, 5, 13, 6, 9, 19) },
				new EnStock { Id = Guid.NewGuid(), CarburantId = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), PrixParLitre = 2, DateMiseAJour = new DateTime(2017, 4, 14, 7, 8, 25) }
			); ;
		}
	}
}
