﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configs
{
    public class ConfigMarque : IEntityTypeConfiguration<Marque>
    {
		public void Configure(EntityTypeBuilder<Marque> builder)
		{
			builder.HasData
			(
				new Marque { Id = Guid.NewGuid(), Libelle = "Total" },
				new Marque { Id = Guid.NewGuid(), Libelle = "Orkan" },
				new Marque { Id = Guid.NewGuid(), Libelle = "Esso" },
				new Marque { Id = Guid.NewGuid(), Libelle = "Sheel" },
				new Marque { Id = Guid.NewGuid(), Libelle = "Avia" }
			); ;
		}
	}
}
