﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configs
{
	public class ConfigCarburant : IEntityTypeConfiguration<Carburant>
	{
		public void Configure(EntityTypeBuilder<Carburant> builder)
		{
			builder.HasData
			(
				new Carburant { Id = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), Nom = "SP95", Code_europeen = "E5" },
				new Carburant { Id = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), Nom = "SP98", Code_europeen = "E5" },
				new Carburant { Id = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), Nom = "SP95 - E10", Code_europeen = "E10" },
				new Carburant { Id = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), Nom = "Superéthanol", Code_europeen = "E85" }
			); ;
		}
	}
}

