﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configs
{
	public class ConfigUtilisateur : IEntityTypeConfiguration<Utilisateur>
	{
		public void Configure(EntityTypeBuilder<Utilisateur> builder)
		{
			builder.HasData
			(
				new Utilisateur { Id = new Guid("3c4233ab-9ebe-493a-aa69-30fd11d1783b"), Login = "Ambroise", Mdp ="1234567890", StationId= new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af") },
				new Utilisateur { Id = new Guid("f1466290-4f48-4a20-8f62-98a64e2e2c05"), Login = "Redouanne", Mdp= "0987654321", StationId= new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af") },
				new Utilisateur { Id = Guid.NewGuid(), Login = "Nathan" , Mdp= "1234567890" , StationId= new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af") },
				new Utilisateur { Id = Guid.NewGuid(), Login = "Test1" , Mdp= "0987654321" , StationId = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e") },
				new Utilisateur { Id = Guid.NewGuid(), Login = "Tomate", Mdp= "1234567890", StationId= new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e") }
			); ;
		}
	}
}
