﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configs
{
	public class ConfigReleve : IEntityTypeConfiguration<Releve>
	{
		public void Configure(EntityTypeBuilder<Releve> builder)
		{
			builder.HasData
			(
				new Releve { Id = Guid.NewGuid(), CarburantId = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), UtilisateurId = new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14"), PrixConstate = 80, DateReleve = new DateTime(2021, 7, 10, 7, 10, 24) },
				new Releve { Id = Guid.NewGuid(), CarburantId = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), StationId = new Guid("03759557-1e0b-40b5-931e-e49e695ce698"), UtilisateurId = new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14"), PrixConstate = 500, DateReleve = new DateTime(2020, 1, 1, 3, 10, 30) },
				new Releve { Id = Guid.NewGuid(), CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), StationId = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"), UtilisateurId = new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14"), PrixConstate = 800, DateReleve = new DateTime(2020, 4, 13, 3, 10, 21) },
				new Releve { Id = Guid.NewGuid(), CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), UtilisateurId = new Guid("73ec1cac-c422-4de1-a28e-30e7c8f76005"), PrixConstate = 20, DateReleve = new DateTime(2019, 5, 13, 8, 9, 19) },
				new Releve { Id = Guid.NewGuid(), CarburantId = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), UtilisateurId = new Guid("73ec1cac-c422-4de1-a28e-30e7c8f76005"), PrixConstate = 20, DateReleve = new DateTime(2017, 4, 13, 7, 1, 25) }
			); ;
		}
	}
}
