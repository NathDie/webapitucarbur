﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configs
{
	public class ConfigStation : IEntityTypeConfiguration<Station>
	{
		public void Configure(EntityTypeBuilder<Station> builder)
		{
			builder.HasData
			(
				new Station { Id = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), Adresse_postale = "59300", Coordonnees = "-48.6318, -21.0266", MarqueId = new Guid("9edd6597-8c7e-4e83-9a60-c82980341347")},
				new Station { Id = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"), Adresse_postale = "59300", Coordonnees = "4.1145, 177.4308", MarqueId = new Guid("9edd6597-8c7e-4e83-9a60-c82980341347")},
				new Station { Id = Guid.NewGuid(), Adresse_postale = "59300", Coordonnees = "-59.4339, 7.3210", MarqueId = new Guid("4b5d2ec5-7297-4617-b109-2e1124d468e8")},
				new Station { Id = Guid.NewGuid(), Adresse_postale = "59300", Coordonnees = "-79.3012, 105.0110", MarqueId = new Guid("4b5d2ec5-7297-4617-b109-2e1124d468e8")},
				new Station { Id = Guid.NewGuid(), Adresse_postale = "59300", Coordonnees = "18.0173, -92.9819", MarqueId = new Guid("4b5d2ec5-7297-4617-b109-2e1124d468e8")}
			); ;
		}
	}
}
