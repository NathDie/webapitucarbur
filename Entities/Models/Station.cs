﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
	public class Station
	{
		[Column("StationId")]
		public Guid Id { get; set; }

		[Required(ErrorMessage = "L'adresse postale est requis.")]
		public string Adresse_postale { get; set; }

		[Required(ErrorMessage = "Les coordonnées géographique sont requis.")]
		public string Coordonnees { get; set; }

		[ForeignKey(nameof(Marque))]
		public Guid MarqueId { get; set; }

	}

}
