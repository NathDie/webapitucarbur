﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
	public class Marque
	{
		[Column("MarqueId")]
		public Guid Id { get; set; }

		[Required(ErrorMessage = "Le nom de de la marque est requis.")]
		[MaxLength(50, ErrorMessage = "La taille maximale pour le nom est de 50 caractères.")]
		public string Libelle { get; set; }

	}

	

}
