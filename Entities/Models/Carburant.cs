﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
	public class Carburant
	{
		[Column("CarburantId")]
		public Guid Id { get; set; }

		[Required(ErrorMessage = "Le nom de du carburant est requis.")]
		[MaxLength(50, ErrorMessage = "La taille maximale pour le nom est de 50 caractères.")]
		[MinLength(50, ErrorMessage = "La taille minimal pour le nom est de 10 caractères.")]
		public string Nom { get; set; }


		[Required(ErrorMessage = "Le code européen est requis.")]
		public string Code_europeen { get; set; }

	}

}
