﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Utilisateur
    {
        [Column("UtilisateurId")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Le login est requis.")]
        [MinLength(5, ErrorMessage = "La taille minimum pour le mot de passe est de 5 caractères.")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Le mot de passe est requis.")]
        [MinLength(10, ErrorMessage = "La taille minimum pour le mot de passe est de 10 caractères.")]
        public string Mdp { get; set; }

        [ForeignKey(nameof(Station))]
        public Nullable<Guid> StationId { get; set; }

    }
}
