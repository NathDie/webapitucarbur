﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
	public class Releve
	{
		[Column("ReleveId")]
		public Guid Id { get; set; }

		[ForeignKey(nameof(Utilisateur))]
		public Guid UtilisateurId { get; set; }

		public Utilisateur utilisateur { get; set; }

		[ForeignKey(nameof(Carburant))]
		public Guid CarburantId { get; set; }

		public Carburant carburant { get; set; }

		[ForeignKey(nameof(Station))]
		public Guid StationId { get; set; }

		public Station station { get; set; }


		[Required(ErrorMessage = "La date du relevé est requise.")]
		public DateTime DateReleve { get; set; }


		[Required(ErrorMessage = "Le prix constaté est requis")]
		public double PrixConstate { get; set; }
		}

}
