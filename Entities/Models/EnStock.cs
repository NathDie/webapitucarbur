﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
	public class EnStock
	{

		[Column("EnStockId")]
		public Guid Id { get; set; }

		[ForeignKey(nameof(Station))]
		public Guid StationId { get; set; }

		[ForeignKey(nameof(Carburant))]
		public Guid CarburantId { get; set; }

		[Required(ErrorMessage = "Le prix par litre  est requis.")]

		public int PrixParLitre { get; set; }


		[Required(ErrorMessage = "La date de dernière mise à jour est requise.")]
		public DateTime DateMiseAJour { get; set; }

	}

}
