﻿using Contrats.Repositories;
using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceLogging;
using Repositories;

namespace NathanAPI
{
	public static class ServiceExtensions
	{
		public static void ConfigureServiceLogging(this IServiceCollection services) => services.AddScoped<ILoggable, Logger>();
		public static void ConfigureGestionRepos(this IServiceCollection services) => services.AddScoped<IGestionRepos, GestionRepos>();
		public static void ConfigureContextSql(this IServiceCollection services, IConfiguration config)
		{
			services.AddDbContext<RepoContext>(opts => opts.UseSqlServer(config.GetConnectionString("sqlConn"), opts => opts.MigrationsAssembly("WebApiTuCarbur")));
		}

		public static void ConfigureCors(this IServiceCollection services) =>
		services.AddCors(options =>
		{
			options.AddPolicy("CorsPolicy", builder =>
			builder.AllowAnyOrigin()
			.AllowAnyMethod()
			.AllowAnyHeader());
		});
	}

}
