﻿using Contrats.Repositories;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLogging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTuCarbur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarqueController : ControllerBase
    {

        private readonly ILoggable _logger;

        private IGestionRepos _context;

        public MarqueController(ILoggable logger, IGestionRepos context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult GetMarques()
        {
            try
            {
                return Ok(_context.Marques.GetMarques(false));
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Marque : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }

        [HttpGet("{id}", Name = "MarqueParId")]
        public IActionResult GetMarque(Guid id)
        {
            try
            {
                Marque marque = _context.Marques.GetMarque(id, false);

                return Ok(marque);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur marque : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }
    }
}
