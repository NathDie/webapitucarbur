﻿using Contrats.Repositories;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLogging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiTuCarbur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReleveController : ControllerBase
    {
        private readonly ILoggable _logger;

        private IGestionRepos _context;

        public ReleveController(ILoggable logger, IGestionRepos context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet("getall")]
        public IActionResult GetReleves()
        {
            try
            {
                return Ok(_context.Releves.GetReleves(false));
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Releve : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }

        [HttpGet("get/{id}", Name = "ReleveParId")]
        public IActionResult GetReleve(Guid id)
        {
            try
            {
                Releve releve = _context.Releves.GetReleve(id, false);

                return Ok(releve);

            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur marque : " + e);
                return StatusCode(500, "Erreur serveuuur");
            }
            }

        

        [HttpGet("station/{id}", Name = "ReleveParStation")]
        public IActionResult GetStation(Guid id)
        {
            try
            {

                List<Releve> releves = _context.Releves.GetReleves(true).ToList();

                

                List<Guid> carburantsId = new List<Guid>();

                

                List<Releve> resultat = new List<Releve>();


                foreach (Releve releve in releves)
                {
                    Console.WriteLine(releve.StationId);
                    if (releve.StationId == id)
                    {
                        Console.WriteLine(releve.StationId);

                        if (!carburantsId.Contains(releve.CarburantId))
                        {
                            resultat.Add(releve);
                            carburantsId.Add(releve.CarburantId);
                        }
                    }
                }


                
                List<Releve> sorted = resultat.OrderBy(Releve => Releve.DateReleve).ToList();


                
                return Ok(sorted);

            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur marque : " + e);
                return StatusCode(500, "Erreur serveeur" + e);
            }
        }


    }
    }