﻿using Contrats.Repositories;
using Entities.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLogging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiTuCarbur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnStockController : ControllerBase
    {
        private readonly ILoggable _logger;

        private IGestionRepos _context;

        public EnStockController(ILoggable logger, IGestionRepos context)
        {
            _logger = logger;
            _context = context;
        }

        public EnStock getStockByStationCarburant( Guid station, Guid carburant)
        {

            EnStock resultat = new EnStock();

            List<EnStock> enStocks = _context.EnStocks.GetEnStocks(false).ToList();
           
            foreach (EnStock enstock in enStocks)
            {
                if (enstock.CarburantId == carburant && enstock.StationId == station)
                {
                    resultat = _context.EnStocks.GetEnStock(enstock.Id, false);
                }

            }
            return resultat; 

        }

        [HttpGet]
        public IActionResult GetEnStocks()
        {
            try
            {
                return Ok(_context.EnStocks.GetEnStocks(false));
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur EnStock : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }

        [HttpGet("{id}", Name = "EnStockParId")]
        public IActionResult GetEnStock(Guid id)
        {
            try
            {
                EnStock enStock = _context.EnStocks.GetEnStock(id, false);

                return Ok(enStock);

            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur marque : " + e);
                return StatusCode(500, "Erreur serveur");
            }
            }

        [HttpPut("majstock")]
        public IActionResult MiseAJourEnStock([FromBody] EnStockUpdateDTO enStockUpdateDTO)
        {
            try
            {
                EnStock enStock = getStockByStationCarburant(enStockUpdateDTO.StationId, enStockUpdateDTO.CarburantId);

                
                enStock.PrixParLitre = enStockUpdateDTO.PrixParLitre;
                enStock.DateMiseAJour = DateTime.Now;

                _context.Save();

                var enStockRetour = new EnStockDTO
                {
                    Id = enStock.Id,
                    StationId= enStock.StationId, 
                    CarburantId = enStock.CarburantId,
                    PrixParLitre= enStock.PrixParLitre,
                    DateMiseAJour= enStock.DateMiseAJour
                };

                return CreatedAtRoute("EnStockParId", new { id = enStockRetour.Id }, enStockRetour);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Update EnStock : " + e);
                return StatusCode(404);
            }
        } 



    }
}