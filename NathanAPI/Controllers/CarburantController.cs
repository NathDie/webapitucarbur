﻿using Contrats.Repositories;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLogging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiTuCarbur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarburantController : ControllerBase
    {
        private readonly ILoggable _logger;

        private IGestionRepos _context;

        public CarburantController(ILoggable logger, IGestionRepos context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult GetCarburants()
        {
            try
            {
                return Ok(_context.Carburants.GetCarburants(false));
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Carburant : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }

         [HttpGet("{id}", Name= "CarburantParId")] 
         public IActionResult GetCarburant(Guid id)
         {
             try
             {
                 Carburant carburant = _context.Carburants.GetCarburant(id, false);

                 return Ok(carburant); 

             } catch (Exception e)
                {
                _logger.LogErreur("Erreur marque : " + e);
                return StatusCode(500, "Erreur serveur");
                }
         }

    }
}
