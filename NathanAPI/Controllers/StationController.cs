﻿using Contrats.Repositories;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using Entities.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLogging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace WebApiTuCarbur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StationController : ControllerBase
    {
        private readonly ILoggable _logger;

        private IGestionRepos _context;

        public int getPriceCarburant(Guid? Station, Guid? Carburant)
        {

            List<EnStock> enStocks = _context.EnStocks.GetEnStocks(false).ToList();

            int price = 0;

            foreach (EnStock enStock in enStocks)
            {
                if (enStock.CarburantId == Carburant && enStock.StationId == Station)
                {
                     price = enStock.PrixParLitre;
                    break;
                }
            }

            return price;
        }

        public DateTime getDateReleve(Guid? Carburant)
        {

            List<Releve> releves = _context.Releves.GetReleves(false).ToList();

           DateTime dateReleve = new DateTime(1900, 7, 10, 7, 10, 24);

            foreach (Releve releve in releves)
            {
                if (releve.CarburantId == Carburant && releve.DateReleve > dateReleve)
                {
                    dateReleve = releve.DateReleve;
                }
            }

            return dateReleve;
        }
        public string GetLibelleMarque(Guid Marque)
        {
            

            Marque marque = _context.Marques.GetMarque(Marque, false);

            string libelle = marque.Libelle;

            return libelle; 
        }
    
        public double GetDistance(string Coord1, string Coord2)
        {

            Coord1.Replace(" ", "");
            string[] coordsStations = Coord1.Split(',');


            coordsStations[1] = coordsStations[1].Replace('.', ',');
            coordsStations[0] = coordsStations[0].Replace('.', ',');


            double latitude = Convert.ToDouble(coordsStations[0]);

            double longitude = Convert.ToDouble(coordsStations[1]);

            Coord2.Replace(" ", "");
            string[] coordsStations2 = Coord2.Split(',');


            coordsStations2[1] = coordsStations2[1].Replace('.', ',');
            coordsStations2[0] = coordsStations2[0].Replace('.', ',');


            double otherLatitude = Convert.ToDouble(coordsStations2[0]);

            double otherLongitude = Convert.ToDouble(coordsStations2[1]);






            var d1 = latitude * (Math.PI / 180.0);
            var num1 = longitude * (Math.PI / 180.0);
            var d2 = otherLatitude * (Math.PI / 180.0);
            var num2 = otherLongitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }


   
        

        public StationController(ILoggable logger, IGestionRepos context)
        {
            _logger = logger;
            _context = context;
        }



        [HttpGet("{id}", Name = "StationParId")]
        public IActionResult GetStation(Guid? id)
        {
            try
            {
                Station station = _context.Stations.GetStation(id, false);

                return Ok(station);

            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur marque : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }


        [HttpGet("details/{id}", Name = "StationDetailParId")]
        public IActionResult GetStationDetails(Guid id)
        {
            try
            {
                Station station = _context.Stations.GetStation(id, false);

                List<CarburantDTO> carburants = new List<CarburantDTO>();

                List<EnStock> enStocks = _context.EnStocks.GetEnStocks(false).ToList();

                foreach (EnStock enstock in enStocks)
                {
                    if (enstock.StationId == station.Id)
                    {
                        Carburant carburant = _context.Carburants.GetCarburant(enstock.CarburantId, false);

                        int price = getPriceCarburant(station.Id, carburant.Id);

                        CarburantDTO carburantdto = new CarburantDTO(carburant.Id, carburant.Nom, carburant.Code_europeen, price); 

                          

                        carburants.Add(carburantdto); 
                    }

                   
                }


               

                string libelle = GetLibelleMarque(station.MarqueId);

                StationDetailDTO sdDTO = new StationDetailDTO(station.Id, station.Adresse_postale, station.Coordonnees, station.MarqueId, libelle, carburants);

                return Ok(sdDTO);

            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur marque : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }

        [HttpGet("bycarburant/{id}", Name = "StationParCarburant")]
        public IActionResult GetStationsByCarburant(Guid id)
        {
            try
            {
                //List<Station> Stations = _context.Stations.GetStations(false).ToList();

                Carburant carburant = _context.Carburants.GetCarburant(id, false);

                List<StationDTO> stations = new List<StationDTO>();

                List<EnStock> enStocks = _context.EnStocks.GetEnStocks(false).ToList();


                foreach (EnStock enstock in enStocks)
                {

                    if (enstock.CarburantId== carburant.Id)
                    {


                      Station station = _context.Stations.GetStation(enstock.StationId, false);

                        string libelle = GetLibelleMarque(station.MarqueId);

                        stations.Add(new StationDTO(station.Id, station.Adresse_postale, station.Coordonnees, station.MarqueId, libelle));
                    }
                }

              
                return Ok(stations);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Station : " + e);
                return StatusCode(500, "Erreur serveurii" + e);
            }
        }


        [HttpGet]
        public IActionResult GetStations()
        {
            try
            {
                List<Station> Stations = _context.Stations.GetStations(false).ToList();

                List<StationDTO> StationsDTO = new List<StationDTO>();

                 foreach (Station station in Stations)
                 {

                    string libelle = GetLibelleMarque(station.MarqueId);

                    StationDTO stationDTO = new StationDTO(station.Id, station.Adresse_postale, station.Coordonnees, station.MarqueId, libelle);

                    StationsDTO.Add(stationDTO);
                 } 
                      




                return Ok(StationsDTO);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Station : " + e);
                return StatusCode(500, "Erreur serveurii" + e);
            }
        }


        [HttpGet("filtrageStation", Name = "GetStationsByPrice")]
        public IActionResult GetStationsFiltre([FromBody] FiltreStationDTO fsDTO)
        {
            try
            {

             

                List<Station> Stations = _context.Stations.GetStations(true).ToList();

                List<StationFiltreDTO> StationsF = new List<StationFiltreDTO>();

                List<EnStock> EnStocks = _context.EnStocks.GetEnStocks(true).ToList();



                    if (fsDTO.CarburantId is not null)
                    {

                        foreach (Station station in Stations)
                        {
                            foreach (EnStock enStock in EnStocks)
                            {
                                if (enStock.CarburantId != fsDTO.CarburantId || enStock.StationId != station.Id)
                                {
                                    Stations.Remove(station);
                                }
                            }
                        }

                    }


                foreach (Station station in Stations)
                {
                    if (fsDTO.Coord is not null)
                    {
                        double distance = GetDistance(fsDTO.Coord, station.Coordonnees);

                        if (fsDTO.DistMax is not null)
                        {
                            if (distance < fsDTO.DistMax)
                            {
                                Stations.Remove(station);
                            }

                        }


                        string libelle = GetLibelleMarque(station.MarqueId);

                        StationFiltreDTO stationF = new StationFiltreDTO(station.Id, station.Adresse_postale, station.Coordonnees, station.MarqueId, distance, libelle);
                        StationsF.Add(stationF);
                    }

                    else
                    {

                        string libelle = GetLibelleMarque(station.MarqueId);

                        StationFiltreDTO stationF = new StationFiltreDTO(station.Id, station.Adresse_postale, station.Coordonnees, station.MarqueId, libelle);
                        StationsF.Add(stationF);

                    }
                }



                if (fsDTO.CarburantId is not null)



                {
                    foreach (StationFiltreDTO stationf in StationsF)
                    {
                        int prixParLitre = getPriceCarburant(stationf.Id, fsDTO.CarburantId);

                        DateTime dateReleve = getDateReleve(fsDTO.CarburantId);

                        stationf.PrixParLitre = prixParLitre;

                        stationf.DateReleve = dateReleve;
                    }
                }



                if (fsDTO.Coord is not null && fsDTO.CarburantId is not null)
                {


                    StationsF.OrderBy(StationFiltreDTO => StationFiltreDTO.Distance).ThenBy(StationFiltreDTO => StationFiltreDTO.PrixParLitre);
                    
                }
                else if (fsDTO.Coord is not null && fsDTO.CarburantId is null)
                {
                    StationsF.OrderBy(StationFiltreDTO => StationFiltreDTO.Distance);
                }

                else if (fsDTO.Coord is null && fsDTO.CarburantId is not null)
                {
                    StationsF.OrderBy(StationFiltreDTO => StationFiltreDTO.PrixParLitre);
                }

                if (fsDTO.UtilisateurId is not null)
                {

                    Utilisateur utilisateur = _context.Utilisateurs.GetUtilisateur((Guid)fsDTO.UtilisateurId, false);


                    Station stationfav = _context.Stations.GetStation(utilisateur.StationId, false);

                    double distanceFav = GetDistance(fsDTO.Coord, stationfav.Coordonnees);


                    string libelle = GetLibelleMarque(stationfav.MarqueId);

                    StationFiltreDTO StationfavF = new StationFiltreDTO(stationfav.Id, stationfav.Adresse_postale, stationfav.Coordonnees, stationfav.MarqueId, distanceFav, libelle);


                    StationsF.Insert(0, StationfavF);

                        }



                



                return Ok(StationsF);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Station : " + e);
                return StatusCode(500, "Erreur serddveur" + e);
            }
        }
 

    }

    }

