﻿using Contrats.Repositories;
using Entities;
using Entities.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLogging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repositories;
using System.Security.Cryptography;
using System.Text;

namespace WebApiTuCarbur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilisateurController : ControllerBase
    {
        private readonly ILoggable _logger;

        private IGestionRepos _context;

        private String hashpassword(string mdp)
        {
            using (var md5 = MD5.Create())
            {
                byte[] passwordBytes = Encoding.ASCII.GetBytes(mdp);

                byte[] hash = md5.ComputeHash(passwordBytes);

                var stringBuilder = new StringBuilder();

                for (int i = 0; i < hash.Length; i++)
                {
                    stringBuilder.Append(hash[i].ToString("X2"));
                }

                return stringBuilder.ToString();
            }
        }

        public UtilisateurController(ILoggable logger, IGestionRepos context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult GetUtilisateurs()
        {
            try
            {
                return Ok(_context.Utilisateurs.GetUtilisateurs(false));
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Utilisateur : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }

        [HttpGet("{id}", Name = "UtilisateurParId")]
        public IActionResult GetUtilisateur(Guid id)
        {
            try
            {
                Utilisateur utilisateur = _context.Utilisateurs.GetUtilisateur(id, false);

                return Ok(utilisateur);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur utilisateur : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }

        [HttpPost("enregistrement")]
        public IActionResult EnregistrementUtilisateur([FromBody] UtilisateurCreationDTO utilisateurCreationDTO)
        {
            if (utilisateurCreationDTO == null)
            {
                _logger.LogErreur("utilisateurCreationDTO reçu du client est null.");
                return BadRequest("Merci de saisir des informations correcte.");
            }

            var utilisateur = new Utilisateur
            {
                Login = utilisateurCreationDTO.Login,
                Mdp = utilisateurCreationDTO.Mdp,
                StationId = new Guid("00000000-0000-0000-0000-000000000000"),
            };

            try
            {
                _context.Utilisateurs.Creer(utilisateur);

                _context.Save();
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Enregistrement utilisateur : " + e);
                return StatusCode(500, "Erreur serveur");
            }

            var utilisateurRetour = new UtilisateurDTO
            {
                Id = utilisateur.Id,
                Login = utilisateur.Login,
                Mdp = utilisateur.Mdp
            };

            return CreatedAtRoute("UtilisateurParId", new { id = utilisateurRetour.Id }, utilisateurRetour);
        }

        [HttpPost("connection")]
        public IActionResult ConnectionUtilisateur([FromBody] UtilisateurConnectionDTO utilisateurConnectionDTO)
        {
            if (utilisateurConnectionDTO == null)
            {
                _logger.LogErreur("utilisateurConnectionDTO reçu du client est null.");
                return BadRequest("Merci de saisir des informations correcte.");
            }

            var login = utilisateurConnectionDTO.Login;
            var mdp = utilisateurConnectionDTO.Mdp;

            try
            {
                Utilisateur utilisateur = _context.Utilisateurs.GetUtilisateurByLoginAndPassword(login, mdp, false);

                return Ok(utilisateur);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Connection utilisateur : " + e);
                return StatusCode(500, "Erreur serveur");
            }
        }


        [HttpPut("changepassword")]
        public IActionResult MiseAJourPasswordUtilisateur([FromBody] UtilisateurUpdateDTO utilisateurUpdateDTO)
        {
            var login = utilisateurUpdateDTO.Login;
            var mdp = utilisateurUpdateDTO.Mdp;

            try
            {
                Utilisateur utilisateur = _context.Utilisateurs.GetUtilisateurByLoginAndPassword(login, mdp, true);

                utilisateur.Login = utilisateurUpdateDTO.Login;
                utilisateur.Mdp = hashpassword(utilisateurUpdateDTO.Mdp);
               

                _context.Save();

                var utilisateurRetour = new UtilisateurDTO
                {
                    Id = utilisateur.Id,
                    Login = utilisateur.Login,
                    Mdp = utilisateur.Mdp,
                    StationId = (Guid)utilisateur.StationId
                   
                };

                return CreatedAtRoute("UtilisateurParId", new { id = utilisateurRetour.Id }, utilisateurRetour);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Update EnStock : " + e);
                return StatusCode(404);
            }
        }

        [HttpPut("changeuser/{id}")]
        public IActionResult MiseAJourUtilisateur(Guid id, [FromBody] UtilisateurUpdateDTO utilisateurUpdateDTO)
        {
            var login = utilisateurUpdateDTO.Login;
            var mdp = utilisateurUpdateDTO.Mdp;

            try
            {
                Utilisateur utilisateur = _context.Utilisateurs.GetUtilisateur(id, true);

                utilisateur.Login = utilisateurUpdateDTO.Login;
                utilisateur.Mdp = hashpassword(utilisateurUpdateDTO.Mdp);


                _context.Save();

                var utilisateurRetour = new UtilisateurDTO
                {
                    Id = utilisateur.Id,
                    Login = utilisateur.Login,
                    Mdp = utilisateur.Mdp,
                    StationId = (Guid)utilisateur.StationId

                };

                return CreatedAtRoute("UtilisateurParId", new { id = utilisateurRetour.Id }, utilisateurRetour);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Update EnStock : " + e);
                return StatusCode(404);
            }
        }



        [HttpPut("modiffav")]
        public IActionResult ModificationFavoris([FromBody] FavoriUpdateDTO favoriUpdateDTO)
        {
            try
            {      
                Utilisateur utilisateur = _context.Utilisateurs.GetUtilisateur(favoriUpdateDTO.fUtilisateurId, true);

                if (utilisateur.StationId == favoriUpdateDTO.StationId)

                {
                    utilisateur.StationId = Guid.Empty;
                    _context.Save();

                }
                else
                {
                    utilisateur.StationId = favoriUpdateDTO.StationId;

                    _context.Save();
                }

            


               

                var utilisateurRetour = new UtilisateurDTO
                {
                    Id = utilisateur.Id,
                    Login = utilisateur.Login,
                    Mdp = utilisateur.Mdp,
                    StationId = (Guid)utilisateur.StationId

                };

                return CreatedAtRoute("UtilisateurParId", new { id = utilisateurRetour.Id }, utilisateurRetour);
            }
            catch (Exception e)
            {
                _logger.LogErreur("Erreur Update EnStock : " + e);
                return StatusCode(404);
            }
        }
    }
}
