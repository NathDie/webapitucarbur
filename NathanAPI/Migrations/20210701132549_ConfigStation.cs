﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApiTuCarbur.Migrations
{
    public partial class ConfigStation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Carburants",
                columns: table => new
                {
                    CarburantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nom = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Code_europeen = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carburants", x => x.CarburantId);
                });

            migrationBuilder.CreateTable(
                name: "Marques",
                columns: table => new
                {
                    MarqueId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Libelle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Marques", x => x.MarqueId);
                });

            migrationBuilder.CreateTable(
                name: "Stations",
                columns: table => new
                {
                    StationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Adresse_postale = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Coordonnees = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MarqueId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stations", x => x.StationId);
                });

            migrationBuilder.CreateTable(
                name: "Utilisateurs",
                columns: table => new
                {
                    UtilisateurId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Login = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Mdp = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StationId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilisateurs", x => x.UtilisateurId);
                });

            migrationBuilder.CreateTable(
                name: "EnStocks",
                columns: table => new
                {
                    EnStockId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CarburantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PrixParLitre = table.Column<int>(type: "int", nullable: false),
                    DateMiseAJour = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnStocks", x => x.EnStockId);
                    table.ForeignKey(
                        name: "FK_EnStocks_Carburants_CarburantId",
                        column: x => x.CarburantId,
                        principalTable: "Carburants",
                        principalColumn: "CarburantId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EnStocks_Stations_StationId",
                        column: x => x.StationId,
                        principalTable: "Stations",
                        principalColumn: "StationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Releve",
                columns: table => new
                {
                    ReleveId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UtilisateurId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CarburantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateReleve = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PrixConstate = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Releve", x => x.ReleveId);
                    table.ForeignKey(
                        name: "FK_Releve_Carburants_CarburantId",
                        column: x => x.CarburantId,
                        principalTable: "Carburants",
                        principalColumn: "CarburantId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Releve_Stations_StationId",
                        column: x => x.StationId,
                        principalTable: "Stations",
                        principalColumn: "StationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Releve_Utilisateurs_UtilisateurId",
                        column: x => x.UtilisateurId,
                        principalTable: "Utilisateurs",
                        principalColumn: "UtilisateurId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Releve",
                columns: new[] { "ReleveId", "CarburantId", "DateReleve", "PrixConstate", "StationId", "UtilisateurId" },
                values: new object[,]
                {
                    { new Guid("23aa5ced-579e-4515-85da-109a8ec028ac"), new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"), new DateTime(2021, 7, 10, 7, 10, 24, 0, DateTimeKind.Unspecified), 80.0, new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14") },
                    { new Guid("73a56a55-bd8c-4676-9216-423cc487d6f9"), new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"), new DateTime(2020, 1, 1, 3, 10, 30, 0, DateTimeKind.Unspecified), 500.0, new Guid("03759557-1e0b-40b5-931e-e49e695ce698"), new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14") },
                    { new Guid("06a8000e-1af4-4dcf-a70b-26ed5b07c298"), new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), new DateTime(2020, 4, 13, 3, 10, 21, 0, DateTimeKind.Unspecified), 800.0, new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"), new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14") },
                    { new Guid("368e087e-7297-40bb-a084-0cb83859767d"), new Guid("fb525059-f556-4188-8641-4c514d4b1d80"), new DateTime(2019, 5, 13, 8, 9, 19, 0, DateTimeKind.Unspecified), 20.0, new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), new Guid("73ec1cac-c422-4de1-a28e-30e7c8f76005") },
                    { new Guid("01482f46-59de-4be7-996a-752b7bffe723"), new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"), new DateTime(2017, 4, 13, 7, 1, 25, 0, DateTimeKind.Unspecified), 20.0, new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"), new Guid("73ec1cac-c422-4de1-a28e-30e7c8f76005") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_EnStocks_CarburantId",
                table: "EnStocks",
                column: "CarburantId");

            migrationBuilder.CreateIndex(
                name: "IX_EnStocks_StationId",
                table: "EnStocks",
                column: "StationId");

            migrationBuilder.CreateIndex(
                name: "IX_Releve_CarburantId",
                table: "Releve",
                column: "CarburantId");

            migrationBuilder.CreateIndex(
                name: "IX_Releve_StationId",
                table: "Releve",
                column: "StationId");

            migrationBuilder.CreateIndex(
                name: "IX_Releve_UtilisateurId",
                table: "Releve",
                column: "UtilisateurId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnStocks");

            migrationBuilder.DropTable(
                name: "Marques");

            migrationBuilder.DropTable(
                name: "Releve");

            migrationBuilder.DropTable(
                name: "Carburants");

            migrationBuilder.DropTable(
                name: "Stations");

            migrationBuilder.DropTable(
                name: "Utilisateurs");
        }
    }
}
