﻿// <auto-generated />
using System;
using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace WebApiTuCarbur.Migrations
{
    [DbContext(typeof(RepoContext))]
    partial class RepoContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.6")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Entities.Models.Carburant", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("CarburantId");

                    b.Property<string>("Code_europeen")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Nom")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Carburants");
                });

            modelBuilder.Entity("Entities.Models.EnStock", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("EnStockId");

                    b.Property<Guid>("CarburantId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("DateMiseAJour")
                        .HasColumnType("datetime2");

                    b.Property<int>("PrixParLitre")
                        .HasColumnType("int");

                    b.Property<Guid>("StationId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("CarburantId");

                    b.HasIndex("StationId");

                    b.ToTable("EnStocks");
                });

            modelBuilder.Entity("Entities.Models.Marque", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("MarqueId");

                    b.Property<string>("Libelle")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Marques");
                });

            modelBuilder.Entity("Entities.Models.Releve", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("ReleveId");

                    b.Property<Guid>("CarburantId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("DateReleve")
                        .HasColumnType("datetime2");

                    b.Property<double>("PrixConstate")
                        .HasColumnType("float");

                    b.Property<Guid>("StationId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("UtilisateurId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("CarburantId");

                    b.HasIndex("StationId");

                    b.HasIndex("UtilisateurId");

                    b.ToTable("Releve");

                    b.HasData(
                        new
                        {
                            Id = new Guid("23aa5ced-579e-4515-85da-109a8ec028ac"),
                            CarburantId = new Guid("a472f76b-664f-47a6-a828-49d3b74f22a0"),
                            DateReleve = new DateTime(2021, 7, 10, 7, 10, 24, 0, DateTimeKind.Unspecified),
                            PrixConstate = 80.0,
                            StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"),
                            UtilisateurId = new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14")
                        },
                        new
                        {
                            Id = new Guid("73a56a55-bd8c-4676-9216-423cc487d6f9"),
                            CarburantId = new Guid("d1144841-7cf2-4839-8f15-7e8479c0d7f6"),
                            DateReleve = new DateTime(2020, 1, 1, 3, 10, 30, 0, DateTimeKind.Unspecified),
                            PrixConstate = 500.0,
                            StationId = new Guid("03759557-1e0b-40b5-931e-e49e695ce698"),
                            UtilisateurId = new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14")
                        },
                        new
                        {
                            Id = new Guid("06a8000e-1af4-4dcf-a70b-26ed5b07c298"),
                            CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"),
                            DateReleve = new DateTime(2020, 4, 13, 3, 10, 21, 0, DateTimeKind.Unspecified),
                            PrixConstate = 800.0,
                            StationId = new Guid("02908ddb-e8b6-4ce3-a81e-e0a113df478e"),
                            UtilisateurId = new Guid("e45d7b7f-d45c-4f48-8d8c-165021369d14")
                        },
                        new
                        {
                            Id = new Guid("368e087e-7297-40bb-a084-0cb83859767d"),
                            CarburantId = new Guid("fb525059-f556-4188-8641-4c514d4b1d80"),
                            DateReleve = new DateTime(2019, 5, 13, 8, 9, 19, 0, DateTimeKind.Unspecified),
                            PrixConstate = 20.0,
                            StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"),
                            UtilisateurId = new Guid("73ec1cac-c422-4de1-a28e-30e7c8f76005")
                        },
                        new
                        {
                            Id = new Guid("01482f46-59de-4be7-996a-752b7bffe723"),
                            CarburantId = new Guid("0cddc54c-04d4-460a-90b3-9eba19dc1b1a"),
                            DateReleve = new DateTime(2017, 4, 13, 7, 1, 25, 0, DateTimeKind.Unspecified),
                            PrixConstate = 20.0,
                            StationId = new Guid("a6c94e1c-6e7f-41d9-999c-d130540d58af"),
                            UtilisateurId = new Guid("73ec1cac-c422-4de1-a28e-30e7c8f76005")
                        });
                });

            modelBuilder.Entity("Entities.Models.Station", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("StationId");

                    b.Property<string>("Adresse_postale")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Coordonnees")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("MarqueId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.ToTable("Stations");
                });

            modelBuilder.Entity("Entities.Models.Utilisateur", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("UtilisateurId");

                    b.Property<string>("Login")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Mdp")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid?>("StationId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.ToTable("Utilisateurs");
                });

            modelBuilder.Entity("Entities.Models.EnStock", b =>
                {
                    b.HasOne("Entities.Models.Carburant", "carburant")
                        .WithMany()
                        .HasForeignKey("CarburantId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Entities.Models.Station", "station")
                        .WithMany()
                        .HasForeignKey("StationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("carburant");

                    b.Navigation("station");
                });

            modelBuilder.Entity("Entities.Models.Releve", b =>
                {
                    b.HasOne("Entities.Models.Carburant", "carburant")
                        .WithMany()
                        .HasForeignKey("CarburantId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Entities.Models.Station", "station")
                        .WithMany()
                        .HasForeignKey("StationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Entities.Models.Utilisateur", "utilisateur")
                        .WithMany()
                        .HasForeignKey("UtilisateurId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("carburant");

                    b.Navigation("station");

                    b.Navigation("utilisateur");
                });
#pragma warning restore 612, 618
        }
    }
}
