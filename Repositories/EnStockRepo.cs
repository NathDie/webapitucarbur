﻿using Entities;
using Contrats;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contrats.Repositories;

namespace Repositories
{
    public class EnStockRepo : RepositoryBase<EnStock>, IEnStockRepo
    {
        public EnStockRepo(RepoContext context) : base(context)
        {

        }

        public IEnumerable<EnStock> GetEnStocks(bool tracked)
        {
            return FindAll(tracked);
        }

        public EnStock GetEnStock(Guid id, bool tracked)
        {
            return FindByCondition(e => e.Id == id, tracked).Single();
        }

        public void Creer(EnStock enStock)
        {
            Create(enStock);
        }

        public void Supprimer(EnStock enStock)
        {
            Delete(enStock);
        }
    }
}
