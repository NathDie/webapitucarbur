﻿using Entities;
using Contrats;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contrats.Repositories;

namespace Repositories
{
    public class ReleveRepo : RepositoryBase<Releve>, IReleveRepo
    {
        public ReleveRepo(RepoContext context) : base(context)
        {

        }

        public IEnumerable<Releve> GetReleves(bool tracked)
        {
            return FindAll(tracked);
        }

        public Releve GetReleve(Guid id, bool tracked)
        {
            return FindByCondition(e => e.Id == id, tracked).Single();
        }

        public void Creer(Releve releve)
        {
            Create(releve);
        }

        public void Supprimer(Releve releve)
        {
            Delete(releve);
        }
    }
}
