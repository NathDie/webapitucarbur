﻿using Entities;
using Contrats;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contrats.Repositories;

namespace Repositories
{
    public class StationRepo : RepositoryBase<Station>, IStationRepo
    {
        public StationRepo(RepoContext context) : base(context)
        {

        }

        public IEnumerable<Station> GetStations(bool tracked)
        {
            return FindAll(tracked);
        }

        public Station GetStation(Guid? id, bool tracked)
        {
            return FindByCondition(e => e.Id == id, tracked).Single();
        }

        public void Creer(Station station)
        {
            Create(station);
        }

        public void Supprimer(Station station)
        {
            Delete(station);
        }
       
    }
}
