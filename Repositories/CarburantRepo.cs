﻿using Entities;
using Contrats;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contrats.Repositories;

namespace Repositories
{
    public class CarburantRepo : RepositoryBase<Carburant>, ICarburantRepo
    {
        public CarburantRepo(RepoContext context) : base(context)
        {

        }

        public IEnumerable<Carburant> GetCarburants(bool tracked)
        {
            return FindAll(tracked);
        }

        public Carburant GetCarburant(Guid id, bool tracked)
        {
            return FindByCondition(e => e.Id == id, tracked).Single();
        }

        public void Creer(Carburant carburant)
        {
            Create(carburant);
        }

        public void Supprimer(Carburant carburant)
        {
            Delete(carburant);
        }
    }
}
