﻿using Entities;
using Contrats;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contrats.Repositories;

namespace Repositories
{
    public class MarqueRepo : RepositoryBase<Marque>, IMarqueRepo
    {
        public MarqueRepo(RepoContext context) : base(context)
        {

        }

        public IEnumerable<Marque> GetMarques(bool tracked)
        {
            return FindAll(tracked);
        }

        public Marque GetMarque(Guid id, bool tracked)
        {
            return FindByCondition(e => e.Id == id, tracked).Single();
        }

        public string GetLabelMarque(Guid id, bool tracked)
        {

            Marque marque = GetMarque(id, tracked);

            string libelle = marque.Libelle;

            return (libelle); 

        }


        public void Creer(Marque marque)
        {
            Create(marque);
        }

        public void Supprimer(Marque marque)
        {
            Delete(marque);
        }
    }
}
