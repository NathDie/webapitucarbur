﻿using Contrats.Repositories;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
	public class GestionRepos : IGestionRepos
	{
		private RepoContext _context;
		private IUtilisateurRepo _utilisateurRepo;
		private IStationRepo _stationRepo;
		private IMarqueRepo _marqueRepo;
		private ICarburantRepo _carburantRepo;
		private IEnStockRepo _enStockRepo;
		private IReleveRepo _releveRepo;


		public GestionRepos(RepoContext context)
		{
			_context = context;
		}

		public IUtilisateurRepo Utilisateurs
		{
			get
			{
				if (_utilisateurRepo == null)
				{
					_utilisateurRepo = new UtilisateurRepo(_context);
				}
				return _utilisateurRepo;
			}
		}


		public IStationRepo Stations
		{
			get
			{
				if (_stationRepo == null)
				{
					_stationRepo = new StationRepo(_context);
				}
				return _stationRepo;
			}
		}

		public ICarburantRepo Carburants
		{
			get
			{
				if (_carburantRepo == null)
				{
					_carburantRepo = new CarburantRepo(_context);
				}
				return _carburantRepo;
			}
		}

		public IEnStockRepo EnStocks
		{
			get
			{
				if (_enStockRepo == null)
				{
					_enStockRepo = new EnStockRepo(_context);
				}
				return _enStockRepo;
			}
		}

		public IReleveRepo Releves
		{
			get
			{
				if (_releveRepo == null)
				{
					_releveRepo = new ReleveRepo(_context);
				}
				return _releveRepo;
			}
		}

		public IMarqueRepo Marques
		{
			get
			{
				if (_marqueRepo == null)
				{
					_marqueRepo = new MarqueRepo(_context);
				}
				return _marqueRepo;
			}
		}

		public void Save() => _context.SaveChanges();

	}

}
