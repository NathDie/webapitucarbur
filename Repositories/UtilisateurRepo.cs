﻿using Entities;
using Contrats;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contrats.Repositories;
using System.Security.Cryptography;

namespace Repositories
{
    public class UtilisateurRepo : RepositoryBase<Utilisateur>, IUtilisateurRepo
    {
        public UtilisateurRepo(RepoContext context) : base(context)
        {

        }

        public IEnumerable<Utilisateur> GetUtilisateurs(bool tracked)
        {
            return FindAll(tracked);
        }

        public Utilisateur GetUtilisateurByLoginAndPassword(String login, String mdp, bool tracked)
        {
            mdp = this.hashpassword(mdp);

            return FindByCondition(u => u.Login == login & u.Mdp == mdp, tracked).Single();
        }



        public Utilisateur GetUtilisateur(Guid id, bool tracked)
        {
            return FindByCondition(e => e.Id == id, tracked).Single();
        }

        public void Creer(Utilisateur utilisateur)
        {
            utilisateur.Mdp = this.hashpassword(utilisateur.Mdp);

            Create(utilisateur);
        }

        public void Supprimer(Utilisateur utilisateur)
        {
            Delete(utilisateur);
        }

        private String hashpassword(string mdp)
        {
            using(var md5 = MD5.Create())
            {
                byte[] passwordBytes = Encoding.ASCII.GetBytes(mdp);

                byte[] hash = md5.ComputeHash(passwordBytes);

                var stringBuilder = new StringBuilder();

                for (int i = 0;i < hash.Length; i++)
                {
                    stringBuilder.Append(hash[i].ToString("X2"));
                }

                return stringBuilder.ToString();
            }
        }
    }
}
